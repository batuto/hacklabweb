#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}" )"
case $1 in
    init)
        echo init
        cp .htaccess ../.htaccess
        if [ ! -d "../pelican" ]; then
            mkdir -p ../pelican/
        else
            rmdir -rf ../pelican/
            mkdir -p ../pelican/
        fi
        if [ ! $(command -v virtualenv) ]; then
            pip install virtualenv
        fi
        virtualenv ../pelican/
        source ../pelican/bin/activate
        pip install pelican markdown

        if [ "$(readlink ../public)" != "hacklabweb/output" ]; then
            rm -rf ../public
            ln -s hacklabweb/output/ ../public
        fi

        pelican -s pelicanconf.py -o output/ -t hacklab_theme content/
        ;;
    recompile)
        echo recompile
        source ../pelican/bin/activate
        pelican -s pelicanconf.py -o output/ -t hacklab_theme content/
        ;;
    fetch-compile)
        echo fetch-compile
        git fetch
        git pull
        source ../pelican/bin/activate
        pelican -s pelicanconf.py -o output/ -t hacklab_theme content/
        ;;
    *)
        echo default behaviour
        echo Help message
        echo Use init to build the environment and compile the pelican content.
        echo Use recompile to recompile the pelican content.
        echo Use fetch-compile to fetch changes and recompile the content.
        ;;
esac

Title: Un hacklab / hackerspace para el estado de Colima	
Date: 2015-09-16
Tags: hackerspace, laboratorio, comunidad
Slug: Colima_HackLab 
Author: Webmaster
Summary: Un nuevo espacio de experimentación para el estado de Colima


Hoy arranca un **nuevo proyecto** en el estado de Colima con el objetivo de habilitar un **espacio en donde personas con intereses en el conocimiento, ciencia, tecnología, arte digital y electrónica podrán encontrar las herramientas necesarias para realizar tareas de investigación, aprendizaje, experimentación e innovación**.

Se trata de un hacklab o hackerspace y un poco de makerspace, **autónomo y libre** que se irá equipando con las contribuciones de los usuarios (donaciones y otras iniciativas o actividades) en el que se fomentará la cultura colaborativa, cooperativa y comunitaria (CO3), que ha de permitir a sus miembros y visitantes el acercamiento a nuevas tecnologías, el desarrollo de nuevos conocimientos y la posibilidad de encontrar ayuda, herramienta e infraestructura para realizar esos proyectos que no han podido ser terminados.
[Hacklab](https://pbs.twimg.com/media/CfFOso8WEAAcsVf.jpg:large)
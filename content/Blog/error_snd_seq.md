Title: Error /dev/snd/seq failed no such device
Date: 2019-03-26
Tags: wiki, error, alsa, audio
Author: rexmalebka
Summary: Como arreglar error con midi


Existe un error al querer utilizar herramientas que manejen ALSA (Advanced Linux Architecture).

En mi caso al querer utilizar el módulo `rimidi` en python para comunicarme con un dispositivo midi.

```
/dev/snd/seq failed no such device
```

En [forum.manjaroo](https://forum.manjaro.org/t/solved-alsa-open-dev-snd-seq-failed-no-such-device/54450) mencionan que el error se corrige instalando `jack2` en lugar de `jack`, en ese caso surgió el error al abrir `qjackctl`.

Reemplacé `jack` por `jack2` y no se corrigió el problema

También mencionan que se trata de permisos de usuario y principalmente si está añadido al grupo `audio`, esto se verifica con el comando `id` , como no estoy en el grupo audio, me añadí con `sudo gpasswd -a $USER audio` , reinicié y aún tenía el problema.

Revisé si existía la dirección `/dev/snd/seq` con `ln -l /dev/snd/seq` al parecer no existía.


```

[rexmalebka@kramble ~]$ ls -l /dev/snd
total 0
drwxr-xr-x  2 root root       80 Mar 26 07:20 by-path
crw-rw----+ 1 root audio 116,  3 Mar 26 07:20 controlC0
crw-rw----+ 1 root audio 116,  2 Mar 26 07:20 controlC29
crw-rw----+ 1 root audio 116,  9 Mar 26 07:20 hwC0D0
crw-rw----+ 1 root audio 116, 10 Mar 26 07:20 hwC0D3
crw-rw----+ 1 root audio 116,  5 Mar 26 07:20 pcmC0D0c
crw-rw----+ 1 root audio 116,  4 Mar 26 07:37 pcmC0D0p
crw-rw----+ 1 root audio 116,  6 Mar 26 07:20 pcmC0D3p
crw-rw----+ 1 root audio 116,  7 Mar 26 07:20 pcmC0D7p
crw-rw----+ 1 root audio 116,  8 Mar 26 07:20 pcmC0D8p
crw-rw----+ 1 root audio 116, 33 Mar 26 07:20 timer
```

En la página de [Documentación de ALSA de arch](https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture#Verifying_correct_sound_modules_are_loaded) mencionan que se carguen los módulos necesarios en caso de no existir.

revisé con `lsmod` :

```
[rexmalebka@kramble ~]$ lsmod  | grep snd_seq
snd_seq_device         16384  0
snd                    77824  21 snd_hda_intel,snd_hwdep,snd_hda_codec,snd_timer,thinkpad_acpi,snd_hda_codec_hdmi,snd_hda_codec_generic,snd_seq_device,snd_hda_codec_realtek,snd_pcm
```

El error se corrigió cargando los módulos snd\_seq con `sudo modprobe snd_seq`:


```
[rexmalebka@kramble ~]$ ls -l /dev/snd/
total 0
drwxr-xr-x  2 root root       80 Mar 26 07:20 by-path
crw-rw----+ 1 root audio 116,  3 Mar 26 07:20 controlC0
crw-rw----+ 1 root audio 116,  2 Mar 26 07:20 controlC29
crw-rw----+ 1 root audio 116,  9 Mar 26 07:20 hwC0D0
crw-rw----+ 1 root audio 116, 10 Mar 26 07:20 hwC0D3
crw-rw----+ 1 root audio 116,  5 Mar 26 07:20 pcmC0D0c
crw-rw----+ 1 root audio 116,  4 Mar 26 07:41 pcmC0D0p
crw-rw----+ 1 root audio 116,  6 Mar 26 07:20 pcmC0D3p
crw-rw----+ 1 root audio 116,  7 Mar 26 07:20 pcmC0D7p
crw-rw----+ 1 root audio 116,  8 Mar 26 07:20 pcmC0D8p
crw-rw----  1 root audio 116,  1 Mar 26 07:48 seq
crw-rw----+ 1 root audio 116, 33 Mar 26 07:20 timer
```

```
[rexmalebka@kramble ~]$ lsmod | grep snd_seq
snd_seq                57344  0
snd_seq_device         16384  1 snd_seq
snd_timer              28672  2 snd_seq,snd_pcm
snd                    77824  22 snd_hda_intel,snd_hwdep,snd_seq,snd_hda_codec,snd_timer,thinkpad_acpi,snd_hda_codec_hdmi,snd_hda_codec_generic,snd_seq_device,snd_hda_codec_realtek,snd_pcm
```

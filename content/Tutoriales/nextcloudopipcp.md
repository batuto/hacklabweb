Title: Instalar NextCloud en OrangePiPCPlus
Date: 2019-03-20 02:00
Slug: instalacion-nextcloud-opipcp
Tags: NextCloud, DIY, OrangePiPCPlus, Armbian
Author: Batuto
Summary: Instalación de NextCloud en una placa OrangePiPCPlus con Armbian como sistema base.
Status: draft

Se comienza con una instalación de ARMBIAN 5.73 stable Debian GNU/Linux 9 (stretch) 4.19.20-sunxi

Lo primero es actualizar la lista de repositorios y los paquetes del sistema con el siguiente comando:

`sudo apt-get update && sudo apt-get upgrade`

Enseguida comenzaremos a instalar y configurar el stack de lo que se conoce como [LAMP](https://es.wikipedia.org/wiki/LAMP).<br>
Primero apache2 y sus utilidades

`sudo apt-get install apache2 apache2-utils`

Nos aseguramos de que apache2 esté ejecutándose, ningún error debería saltar.

`sudo systemctl restart apache2`

Para asegurarnos de que el servicio corra en cada reinicio del equipo habilitaremos dicha función con el siguiente comando

`sudo systemctl enable apache2.service`

Podemos verificar su funcionamiento en el navegador al ingresar su dirección IP en la barra de direcciones

![It works!]({attach}images/apache2default.png)

Y para terminar la configuración haremos cambios a los permisos correspondientes.

`sudo chown www-data /var/www/html/ -R`

Ahora sigue la instalación y configuración de MariaDB, para que NextCloud pueda trabajar correctamente con una base de datos.
Instalación

`sudo apt-get install mariadb-server mariadb-client`

Nos aseguramos de que el servicio esté trabajando

`sudo systemctl restart mysql.service`

Y permitimos que se ejecute en cada reinicio

`sudo systemctl enable mysql.service`

Y para finalizar esta instalación necesitamos correr el siguiente comando que nos ayudará a asegurar la configuración inicial.
Leer con atención!

`sudo mysql_secure_installation`

Al final podremos leer un mensaje de agradecimiento

```
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

You already have a root password set, so you can safely answer 'n'.

Change the root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```


El último paquete a instalar es el que permite básicamente, hacer funcionar a NextCloud, se trata de PHP

`sudo apt-get install php7.0-fpm php7.0-mysql php7.0-common php7.0-gd php7.0-json php7.0-cli php7.0-curl libapache2-mod-php7.0 php7.0-zip php7.0-xml php7.0-mbstring` 

Una vez instalado, debemos asegurarnos de que el módulo de PHP para Apache está activado.

`sudo a2enmod php7.0 && sudo service apache2 restart`

Lo siguiente es probar que PHP está funcionando en Apache
Creamos un archivo con un saludo, como el usuario www-data 

`sudo -u www-data sh -c 'echo "<?php print(\"Hola HackLab!\"); ?>"  > /var/www/html/test.php'`

Y si lo anterior funciona podremos leer el mensaje en 
http://REEMPLAZAR_x_IP_DEL_DISPOSITIVO/test.php

Ahora que todo está listo para asentar a NextCloud
necesitamos descargarlo. En [este enlace](https://nextcloud.com/install/#instructions-server) podemos obtener la última versión.
En mi caso descargo y extraigo directamente en el directorio.

```
cd /var/www/html/
sudo wget https://download.nextcloud.com/server/releases/nextcloud-15.0.5.zip
sudo unzip nextcloud-15.0.5.zip
sudo rm nextcloud-15.0.5.zip
sudo chown www-data:www-data nextcloud/ -R
```

Ahora podemos ver la siguiente pantalla de configuración

![NextCloud]({attach}images/ncinitialconfig.png)

Tenemos que crear una base de datos, para eso nos logueamos a MariaDB como el usuario root haciendo uso de la contraseña que fijamos anteriormente.

`mysql -u root -p`

Al acceder creamos una base de datos con el nombre deseado

`create database nextcloud_testbd;`

y además un usuario con su respectiva contraseña

`create user nctestuser@localhost identified by 'AlgocomoPassword';`

y darle permisos

`grant all privileges on nextcloud_testbd.* to nctestuser@localhost identified by 'AlgocomoPassword';`

limpiamos los privilegios

`flush privileges;`

salimos

`exit;`

Hasta aquí podríamos decir que el servidor de NextCloud está listo para ser configurado según nuestras necesidades.
Pasar a [iniciando en NextCloud](#inicio_nc)

Ahora, la parte un poco complicada y tal vez no necesaria si no dispondremos de más servicios en nuestro servidor web.
Crear un host virtual.

Necesitamos crear un archivo de configuración

`sudo nano /etc/apache2/sites-available/nextcloud.conf`

con el siguiente contenido:

```
<VirtualHost *:80>
 DocumentRoot "/var/www/nextcloud"
 ServerName nextcloud.tu-dominio.com

 ErrorLog ${APACHE_LOG_DIR}/error.log
 CustomLog ${APACHE_LOG_DIR}/access.log combined

<Directory /var/www/nextcloud/>
 Options +FollowSymlinks
 AllowOverride All

 <IfModule mod_dav.c>
 Dav off
 </IfModule>

 SetEnv HOME /var/www/nextcloud
 SetEnv HTTP_HOME /var/www/nextcloud
 Satisfy Any

</Directory>

</VirtualHost>
```

Si leíste la configuración tal vez te des cuenta que hay algo raro, respecto a los pasos anteriores, el directorio de configuración, aquí al crear el host virtual sí nos interesa su ubicación, entonces necesitaremos moverlo.

`sudo mv /var/www/html/nextcloud /var/www/`

Creamos un enlace a nuestro recién creado fichero para habilitar el sitio.

`sudo ln -s /etc/apache2/sites-available/nextcloud.conf /etc/apache2/sites-enabled/nextcloud.conf`

Habilitamos los módulos de apache2 para asegurarnos de que todo está correcto.
`sudo a2enmod rewrite headers env dir mime setenvif`

Reiniciamos apache2

`sudo systemctl restart apache2`

En este punto si no disponemos de el dominio que configuramos podemos agregarlo a nuestro archivo de hosts en los equipos cliente de la red local para que el acceso nos resulte más cómodo.


<a name="inicio_nc"></a> Iniciando la configuración de NextCloud


Añadiendo seguridad con let's enctypt

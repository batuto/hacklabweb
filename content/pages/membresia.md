Title: Membresía
Slug: membresia

# Coopera, colabora y solidarízate

---

Si te interesa apoyar o ser parte del HackLab, tenemos una buena y una mala noticia (hablando tradicionalmente):
**«SE TERMINARON LAS MEMBRESÍAS»**.

<p class="text-justify">Hemos decidido apostar por un espacio sustentado y soportado especialmente por una cultura solidaria, cooperativa y colaborativa, que hasta el día de hoy y sin darnos cuenta hemos venido haciendo desde que arrancamos. Jamás hemos negado el acceso tanto a nuestro espacio como a nuestras actividades por falta de una aportación económica y así vamos a seguir consolidando nuestro espacio y actividades.</p>

## Así que si te gusta lo que hacemos y quieres venir a hacer más con nosotrxs o tu deseo es apoyar:

* Conócenos y súmate a la causa.
* Participa en nuestros eventos y convocatorias.
* Coopera, colabora y solidarízate. 

## ¿Cómo o con qué puedo cooperar y/o colaborar?

**Monetariamente:** Esperamos algún día lograr plena autonomía, especialmente monetaria, sinembargo mes con mes hay algunas cuentas como las de servicios básicos o herramientas que solo en efectivo podemos liquidar. Por ende cualquier aportación económica es de gran ayuda.

**En especie:** Habitualmente en las actividades del HackLab tratamos de amenizar al paladar, el estomago o calmar la sed. Así que quizás puedas cooperar con agua, galletas, fruta, etc..

**Trabajo:** Todos tenemos alguna habilidad o profesión que podemos explotar para mejorar el espacio y sus actividades, puede ser un poco de ayuda con herrería, diseño, carpintería, etc..

**Conocimiento:** Si eres alguien que sabe mucho o ha aprendido algo de alguna herramienta de software, hardware o técnica, también puedes ayudar con alguna plática o taller compartiendo lo que sabes, seguro aprenderás un poco más de quien no sabe y entonces así generamos una cadena de reciprocidad.

**Donando:** En ocasiones puedes tener PCs, componentes, sillas, mesas, gavetas, etc. que no utilizas y que seguramente serán de mucha utilidad en el HackLab. 

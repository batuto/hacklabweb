#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Batuto Nope!'
SITENAME = u'Colima HackLab'
SITEURL = 'https://batuto.blah.hacklab.cc'
SITELOGO = '/theme/images/hacklab.cc.png'
SITEICON = '/theme/images/hacklab.cc_icon.png'

PATH = 'content'
THEME = 'hacklab_theme'
ARTICLE_SAVE_AS = '{date:%Y}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{slug}.html'
YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'

TIMEZONE = 'America/Mexico_City'

DEFAULT_LANG = u'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://python.org/'),
         ('Jinja2', 'https://jinja.pocoo.org/'),
         ('Telegram', 'https://telegram.org/'),)

# Social widget
SOCIAL = (('GNU Social', 'https://gnu.io'),
          ('diaspora*', 'https://joindiaspora.com/'),
          ('Mastodon', 'https://mastodon.social'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False

# Destructive option to remove all the old content before creating the new.
DELETE_OUTPUT_DIRECTORY = True
# A list of filenames that should be retained and not deleted from the output directory. One use case would be the preservation of version control data.
# Example:
# OUTPUT_RETENTION = [".hg", ".git", ".bzr"]

DISPLAY_PAGES_ON_MENU = True
# Whether to display pages on the menu of the template. Templates may or may not honor this setting.

SUMMARY_MAX_LENGTH = 25


MENUITEMS = [('Home', '/'), ]
# A list of tuples (Title, URL) for additional menu items to appear at the beginning of the main menu.

PLUGIN_PATHS = ["plugins"]
PLUGINS = ["tag_cloud"]
